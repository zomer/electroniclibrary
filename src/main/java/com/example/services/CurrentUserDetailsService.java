package com.example.services;

import com.example.models.CurrentUser;
import com.example.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CurrentUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public CurrentUser loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userService.findByEmail(email) ;

        if (user == null) {
            throw  new UsernameNotFoundException(String.format("User with email=%s was not found", email));
        }

        return new CurrentUser(user);
    }
}
