package com.example.services;

import com.example.models.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BookService {

    Book save(Book book);

    Book findById(long id);

    void delete(long id);

    Iterable<Book> all();

    Page<Book> getPage(Pageable pageable);

    Page<Book> getFilterPage(String title, Book.SearchTags searchTag, Pageable pageable);
}
