package com.example.services;

import com.example.models.Publisher;
import com.example.repositories.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PublisherServiceImp implements PublisherService {

    @Autowired
    private PublisherRepository publisherRepository;

    @Override
    public Publisher save(Publisher publisher) {
        return publisherRepository.save(publisher);
    }

    @Override
    public Publisher findById(long id) {
        return publisherRepository.findOne(id);
    }

    @Override
    public void delete(long id) {
        publisherRepository.delete(id);
    }

    @Override
    public Iterable<Publisher> all() {
        return publisherRepository.findAll();
    }
}
