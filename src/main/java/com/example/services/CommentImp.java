package com.example.services;

import com.example.components.DateFormater;
import com.example.models.Book;
import com.example.models.Comment;
import com.example.models.User;
import com.example.repositories.CommentRepository;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class CommentImp implements CommentService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private BookService bookService;

    @Autowired
    private UserService userService;


    @Override
    public Comment save(Comment book) {
        return commentRepository.saveAndFlush(book);
    }

    @Override
    public JSONObject add(long bookId, String comment, long userId) {

        JSONObject obj = new JSONObject();

        Book book = bookService.findById(bookId);
        if (book == null) {
            log.error("CommentImp add() method, book empty [id] = " + bookId);
            obj.put("post_status", "error");
            return obj;
        }

        User user = userService.findById(userId);
        if (user == null ) {
            log.error("CommentImp add() method, user empty [id] = " + userId);
            obj.put("post_status", "error");
            return obj;
        }

        Comment commentItem =  new Comment(new Date(), comment, user);
        save(commentItem);

        book.getComments().add(commentItem);
        bookService.save(book);

        obj.put("post_date", DateFormater.simpleFormat(commentItem.getPostDate()));
        obj.put("post_text", commentItem.getText());
        obj.put("post_user_nick", commentItem.getUser().getNick());
        obj.put("post_count", book.getComments().size());
        obj.put("post_status", "ok");
        return obj;
    }

    @Override
    public void delete(long id) {
        commentRepository.delete(id);
    }
}
