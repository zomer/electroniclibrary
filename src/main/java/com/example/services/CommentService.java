package com.example.services;

import com.example.models.Book;
import com.example.models.Comment;
import org.json.simple.JSONObject;

public interface CommentService {

    Comment save(Comment comment);

    JSONObject add(long bookId, String comment, long userId);

    void delete(long id);
}
