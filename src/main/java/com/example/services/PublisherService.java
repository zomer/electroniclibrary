package com.example.services;

import com.example.models.Publisher;

public interface PublisherService {

    Publisher save(Publisher publisher);

    Publisher findById(long id);

    void delete(long id);

    Iterable<Publisher> all();
}
