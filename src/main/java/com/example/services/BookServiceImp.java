package com.example.services;

import com.example.models.Book;
import com.example.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImp implements BookService {

    private static final int PAGE_SIZE = 4;

    @Autowired
    private BookRepository bookRepository;

    @Override
    public Book save(Book book) {
        return bookRepository.save(book);
    }

    @Override
    public void delete(long id) {
        bookRepository.delete(id);
    }

    @Override
    public Iterable<Book> all() {
        return bookRepository.findAll();
    }

    @Override
    public Page<Book> getPage(Pageable pageable) {
        return bookRepository.findAll(pageable);
    }

    @Override
    public Page<Book> getFilterPage(String title, Book.SearchTags searchTag, Pageable pageable) {
        return bookRepository.findByTitleLikeAndBySearchTag(title, searchTag, pageable);
    }

    @Override
    public Book findById(long id) {
        return bookRepository.findOne(id);
    }
}
