package com.example.services;

import com.example.forms.UserCreateForm;
import com.example.models.User;

import java.util.Collection;
import java.util.Optional;

public interface UserService {
    User findById(long id);

    User findByEmail(String email);

    User findByNick(String nick);

    User create(UserCreateForm form);

    Collection<User> all();
}
