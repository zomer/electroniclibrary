package com.example.services;

import com.example.models.Author;

public interface AuthorService {

    Author save(Author book);

    Author findById(long id);

    void delete(long id);

    Iterable<Author> all();
}
