package com.example.services;

import com.example.models.Author;
import com.example.repositories.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorServiceImp implements AuthorService {

    @Autowired
    private AuthorRepository authorRepository;

    @Override
    public Author save(Author author) {
        return authorRepository.save(author);
    }

    @Override
    public Author findById(long id) {
        return authorRepository.findOne(id);
    }

    @Override
    public void delete(long id) {
        authorRepository.delete(id);
    }

    @Override
    public Iterable<Author> all() {
        return authorRepository.findAll();
    }
}
