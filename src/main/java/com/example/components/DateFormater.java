package com.example.components;

import groovy.lang.Singleton;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormater {

    public static String simpleFormat (Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(date);
    }
}
