package com.example.components;

import com.example.forms.UserCreateForm;
import com.example.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class UserCreateFormValidator implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UserCreateForm.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserCreateForm form = (UserCreateForm) target;
        validatePasswords(errors, form);
        validateEmail(errors, form);
        validateNick(errors, form);
    }

    private void validatePasswords(Errors errors, UserCreateForm form) {
        if (!form.getPassword().equals(form.getPasswordRepeated())) {
            errors.rejectValue("passwordRepeated", "validation.message.password.do.notmatch");
        }
    }

    private void validateEmail(Errors errors, UserCreateForm form) {
        if (userService.findByEmail(form.getEmail()) != null) {
            errors.rejectValue("email", "validation.message.user.email.exist");
        }
    }

    private void validateNick(Errors errors, UserCreateForm form) {
        if (userService.findByNick(form.getNick()) != null) {
            errors.rejectValue("nick", "validation.message.user.nick.exist");
        }
    }
}
