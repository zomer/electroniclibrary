package com.example.controllers;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;

@RestController
public class FileUploadController {

    private static final String ROOT = "upload";
    private static final int MAX_SIZE_KB = 20971520;


    private final ResourceLoader resourceLoader;

    @Autowired
    public FileUploadController(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/admin/upload")
    public String uploadfile(@RequestParam("uploadfile") MultipartFile file) {

        JSONObject obj = new JSONObject();

        if (!file.isEmpty() && file.getSize() < MAX_SIZE_KB) {
            try {
                String name = URLEncoder.encode(Calendar.getInstance().getTime().getTime() + file.getOriginalFilename(), "UTF-8");
                Path path = Paths.get(ROOT, name);
                obj.put("name", name);
                Files.copy(file.getInputStream(), path);
            } catch (IOException | RuntimeException e) {
                obj.put("error", "Load Exception");
            }
        } else {
            obj.put("error", "File empty or have more max size");
        }
        return obj.toJSONString();
    }

    @RequestMapping(method = RequestMethod.GET, value = "show/{filename:.+}")
    @ResponseBody
    public ResponseEntity<?> showImage(@PathVariable String filename) {

        try {
            String path = Paths.get(ROOT, filename).toString();
            return ResponseEntity.ok(resourceLoader.getResource("file:" + path));
        } catch (Exception e) {
            return ResponseEntity.ok(resourceLoader.getResource("static/img/testimg.png"));
        }
     }

    @RequestMapping(method = RequestMethod.GET, value = "download/{filename:.+}")
    @ResponseBody
    public ResponseEntity<?> download(@PathVariable String filename) throws IOException {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        Resource pdfFile = resourceLoader.getResource("file:" + Paths.get(ROOT, filename).toString());
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentLength(pdfFile.contentLength())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(new InputStreamResource(pdfFile.getInputStream()));
    }
}
