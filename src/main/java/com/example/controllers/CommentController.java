package com.example.controllers;

import com.example.models.Book;
import com.example.services.BookService;
import com.example.services.CommentService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class CommentController {

    @Autowired
    private CommentService commentService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.POST, value = "comment/add")
    public String add(@RequestParam(value="book_id") long id,
                      @RequestParam(value="comment") String comment,
                      @RequestParam(value="user_id") long userId) {

        return commentService.add(id, comment, userId).toJSONString();
    }
}
