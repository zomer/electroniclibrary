package com.example.controllers;

import com.example.components.UserCreateFormValidator;
import com.example.forms.UserCreateForm;
import com.example.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserCreateFormValidator userCreateFormValidator;

    @InitBinder("user")
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(userCreateFormValidator);
    }

    @RequestMapping("/users")
    public ModelAndView users() {
        return new ModelAndView("/admin/user/list", "users", userService.all());
    }

    @RequestMapping(value = "user/create", method = RequestMethod.POST)
    public String doCreate(@Valid  @ModelAttribute("user") UserCreateForm userForm, BindingResult result) {

        if (result.hasErrors()) {
            return "/admin/user/create";
        }

        userService.create(userForm);

        return "redirect:/admin/users";
    }

    @RequestMapping(value = "/user/create", method = RequestMethod.GET)
    public String create(Model model) {
        model.addAttribute("user", new UserCreateForm());
        return "/admin/user/create";
    }
}
