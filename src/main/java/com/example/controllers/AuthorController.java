package com.example.controllers;

import com.example.exeptions.ItemNotFoundException;
import com.example.models.Author;
import com.example.services.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin")
public class AuthorController {

    @Autowired
    private AuthorService authorService;

    @RequestMapping(value = "authors", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("authors", authorService.all());
        return "/admin/author/list";
    }

    @RequestMapping(value = "author/add", method = RequestMethod.GET)
    public String add(Model model) {
        model.addAttribute("author", new Author());
        return "/admin/author/edit";
    }

    @RequestMapping(value = "author/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable long id, Model model) {

        Author author = authorService.findById(id);

        if (author == null) {
            throw new ItemNotFoundException("edit() author with id [" + id + "] not found");
        }

        model.addAttribute("author", authorService.findById(id));
        return "/admin/author/edit";
    }

    @RequestMapping(value = "author/save", method = RequestMethod.POST)
    public String save(@Valid Author author, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "/admin/author/edit";
        }

        Long authorId = author.getId();

        if (authorId != null && authorService.findById(authorId) == null) {
            throw new ItemNotFoundException("save() author with id [" + authorId + "] not found");
        }

        authorService.save(author);
        return "redirect:/admin/authors";
    }

    @RequestMapping("author/delete/{id}")
    public String delete(@PathVariable long id) {

        if (authorService.findById(id) == null) {
            throw new ItemNotFoundException("delete() author with id [" + id + "] not found");
        }

        authorService.delete(id);
        return "redirect:/admin/users";
    }
}
