package com.example.controllers;

import com.example.exeptions.ItemNotFoundException;
import com.example.models.Book;
import com.example.models.Publisher;
import com.example.services.AuthorService;
import com.example.services.BookService;
import com.example.services.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin")
public class BookController {

    @Autowired
    private BookService bookService;

    @Autowired
    private PublisherService  publisherService;

    @Autowired
    private AuthorService authorService;

    @RequestMapping(value = "books", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("books", bookService.all());
        return "/admin/book/list";
    }

    @RequestMapping(value = "book/add", method = RequestMethod.GET)
    public String add(Model model) {
        model.addAttribute("book", new Book());
        model.addAttribute("bookPublishers", publisherService.all());
        model.addAttribute("bookAuthors", authorService.all());
        return "/admin/book/edit";
    }

    @RequestMapping(value = "book/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable long id, Model model) {

        Book book = bookService.findById(id);

        if (book == null) {
            throw new ItemNotFoundException("edit() book with id [" + id + "] not found");
        }

        model.addAttribute("bookPublishers", publisherService.all());
        model.addAttribute("bookAuthors", authorService.all());
        model.addAttribute("book", book);
        return "/admin/book/edit";
    }

    @RequestMapping(value = "book/save", method = RequestMethod.POST)
    public String save(@Valid Book book, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("bookPublishers", publisherService.all());
            model.addAttribute("bookAuthors", authorService.all());
            return "/admin/book/edit";
        }

        Long bookId = book.getId();

        if (bookId != null && bookService.findById(bookId) == null) {
            throw new ItemNotFoundException("save() book with id [" + bookId + "] not found");
        }

        bookService.save(book);
        return "redirect:/admin/books";
    }

    @RequestMapping("book/delete/{id}")
    public String delete(@PathVariable long id) {

        if (bookService.findById(id) == null) {
            throw new ItemNotFoundException("delete() book with id [" + id + "] not found");
        }

        bookService.delete(id);
        return "redirect:/admin/books";
    }
}
