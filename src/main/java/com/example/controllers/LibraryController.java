package com.example.controllers;

import com.example.components.PageWrapper;
import com.example.exeptions.ItemNotFoundException;
import com.example.models.Book;
import com.example.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import static java.util.stream.Collectors.toList;

@Controller
public class LibraryController {

    @Autowired
    private BookService bookService;

    @RequestMapping(method = RequestMethod.GET, value = "/")
    String index(Model model, @PageableDefault(page = 0, size = 4) Pageable pageable) {
        Page bookPage = bookService.getPage(pageable);
        PageWrapper<Book> page = new PageWrapper<Book>(bookPage, "/");
        model.addAttribute("page", page);
        return "index";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/")
    String search(Model model, @PageableDefault(page = 0, size = 4) Pageable pageable,
                  @RequestParam("title")String title,
                  @RequestParam("searchTag")Book.SearchTags searchTag) {

        Page bookPage;

        if (title.isEmpty() && searchTag == null) {
            bookPage = bookService.getPage(pageable);
        } else {
            bookPage = bookService.getFilterPage(title, searchTag, pageable);
        }

        PageWrapper<Book> page = new PageWrapper<Book>(bookPage, "/");
        model.addAttribute("page", page);
        model.addAttribute("title", title);
        model.addAttribute("searchTag", searchTag);
        return "index";
    }


    @RequestMapping(method = RequestMethod.GET, value = "book/show/{id}")
    public String showBook(@PathVariable long id, Model model) {

        Book book = bookService.findById(id);

        if (book == null) {
            throw new ItemNotFoundException("edit() book with id [" + id + "] not found");
        }

        model.addAttribute("book", book);
        model.addAttribute("bookComments", book.getComments().stream().sorted((date1, date2) ->  date1.getPostDate().compareTo(date2.getPostDate()) * -1).collect(toList()));
        return "/frontend/showBook";
    }
}
