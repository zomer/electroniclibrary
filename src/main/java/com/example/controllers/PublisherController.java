package com.example.controllers;

import com.example.exeptions.ItemNotFoundException;
import com.example.models.Publisher;
import com.example.services.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin")
public class PublisherController {

    @Autowired
    private PublisherService publisherService;

    @RequestMapping(value = "publishers", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("publishers", publisherService.all());
        return "/admin/publisher/list";
    }

    @RequestMapping(value = "publisher/add", method = RequestMethod.GET)
    public String add(Model model) {
        model.addAttribute("publisher", new Publisher());
        return "/admin/publisher/edit";
    }

    @RequestMapping(value = "publisher/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable long id, Model model) {

        Publisher publisher = publisherService.findById(id);

        if (publisher == null) {
            throw new ItemNotFoundException("edit() publisher with id [" + id + "] not found");
        }

        model.addAttribute("publisher", publisherService.findById(id));

        return "/admin/publisher/edit";
    }

    @RequestMapping(value = "publisher/save", method = RequestMethod.POST)
    public String save(@Valid Publisher publisher, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "/admin/publisher/edit";
        }

        Long publisherId = publisher.getId();

        if (publisherId != null && publisherService.findById(publisherId) == null) {
            throw new ItemNotFoundException("save() publisher with id [" + publisherId + "] not found");
        }

        publisherService.save(publisher);
        return "redirect:/admin/publishers";
    }

    @RequestMapping("publisher/delete/{id}")
    public String delete(@PathVariable long id) {

        if (publisherService.findById(id) == null) {
            throw new ItemNotFoundException("delete() publisher with id [" + id + "] not found");
        }

        publisherService.delete(id);
        return "redirect:/admin/publishers";
    }
}
