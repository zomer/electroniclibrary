package com.example.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CustomErrorController {

    @RequestMapping(value = "/exception")
    public ModelAndView error(Exception exception) {
        ModelAndView modelAndView = new ModelAndView("errors/general");
        modelAndView.addObject("exception", exception);
        return modelAndView;
    }

    @RequestMapping(value = "/500")
    public ModelAndView internal() {
        ModelAndView modelAndView = new ModelAndView("errors/500");
        return modelAndView;
    }

    @RequestMapping(value = "/401")
    public ModelAndView unauthorized() {
        ModelAndView modelAndView = new ModelAndView("errors/401");
        return modelAndView;
    }

    @RequestMapping(value = "/404")
    public ModelAndView notFound() {
        ModelAndView modelAndView = new ModelAndView("errors/404");
        return modelAndView;
    }
}
