package com.example.repositories;

import com.example.models.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface BookRepository extends PagingAndSortingRepository<Book, Long> {
    @Query("from Book b where b.title LIKE :title OR :searchTag  MEMBER OF b.searchTags")
    Page<Book> findByTitleLikeAndBySearchTag(@Param("title") String title, @Param("searchTag") Book.SearchTags searchTag, Pageable pageable);
}
