package com.example.models;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Book extends Model {

    @NotNull
    @NotEmpty
    private String title;

    @NotNull
    @NotEmpty
    private String description;

    @NotNull
    @NotEmpty
    private String img;

    @NotNull
    @NotEmpty
    private String doc;

    @ManyToMany
    @JoinTable(
            name="book_author",
            joinColumns={@JoinColumn(name="BOOK_ID", referencedColumnName="ID")},
            inverseJoinColumns={@JoinColumn(name="AUTHOR_ID", referencedColumnName="ID")})
    private List<Author> authors = new ArrayList<>();

    @ManyToOne
    private Publisher publisher;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Comment> comments = new ArrayList<>();

    @ElementCollection(targetClass = SearchTags.class)
    @Enumerated(EnumType.STRING)
    private List<SearchTags> searchTags = new ArrayList<>();

    public List<SearchTags> getSearchTags() {
        return searchTags;
    }

    public void setSearchTags(List<SearchTags> searchTags) {
        this.searchTags = searchTags;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public enum SearchTags {
        SPORT, SAFE, FANNY
    }
}
