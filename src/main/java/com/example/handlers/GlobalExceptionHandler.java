package com.example.handlers;

import com.example.exeptions.ItemNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ModelAndView exception(Exception exception, HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();

        if (request.isUserInRole("ADMIN")) {
            modelAndView.setViewName("errors/general");
            modelAndView.addObject("exception", exception);
        } else {
            modelAndView.setViewName("errors/404");
        }
        return modelAndView;
    }

    @ExceptionHandler(value = ItemNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ModelAndView itemNotFound(Exception exception, HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();

       if (request.isUserInRole("ADMIN")) {
           modelAndView.setViewName("errors/general");
           modelAndView.addObject("exception", exception);
        } else {
           modelAndView.setViewName("errors/404");
       }

        return modelAndView;
    }
}
