(function($) {
    var commentError = $('#comment-error');
    var frm = $('#comment-form');
    var commentsCount = $('#comments-count');

    frm.submit(function (ev) {
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            success: function (data) {
                var json = $.parseJSON(data);

                if (json.post_status === "error" || data.post_status === "") {
                    commentError.show();
                }

                if (json.post_status === "ok") {
                    commentsCount.html(json.post_count + " comments");
                    $("#comments").prepend('<div class="media">' +
                                            '<p class="pull-right"><small>Post date '+ json.post_date +'</small></p>' +
                                            '<div class="media-body">' +
                                            '<h4 class="media-heading user_name">'+ json.post_user_nick +'</h4>' +
                                            '<span>'+ json.post_text +'</span>' +
                                            '</div>' +
                                            '</div>');
                }
            },
            error: function (error) {
                commentError.show();
            }
        });

        ev.preventDefault();
    });
})(jQuery);