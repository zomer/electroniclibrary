$("#img-file").fileinput({
    uploadUrl: "/admin/upload",
    allowedFileExtensions: ["jpg", "png", "gif"],
    maxImageWidth: 800,
    maxImageHeight: 600,
    maxFileCount: 1,
    minFileCount: 1,
    autoReplace: true,
    uploadAsync: false,
    showRemove: false,
    showUploadedThumbs: false,
    showUpload: false,
    validateInitialCount: true,
    overwriteInitial: true,
    fileActionSettings: {
        showRemove: false,
        showZoom: false,
        showDrag: false

    },
    maxFileSize: 20000,
    initialPreview: [initialPreviewImgData($("#img-file").data('img'))]
}).on("filebatchuploadsuccess", function(event, data, previewId, index) {
    $('[name="' +  $(this).data('field') + '"]').val(data.response.name);
}).on('filesuccessremove', function(event, id) {
    $('[name="' +  $(this).data('field') + '"]').val("");
}).on("filebatchselected", function(event, files) {
    $("#img-file").fileinput("upload");
});

$("#doc-file").fileinput({
    uploadUrl: "/admin/upload",
    allowedFileExtensions: ["txt", "rtf", "doc", "odt", "pdf"],
    maxFileCount: 1,
    minFileCount: 1,
    uploadAsync: false,
    showRemove: false,
    autoReplace: true,
    showUploadedThumbs: false,
    showUpload: false,
    validateInitialCount: true,
    overwriteInitial: true,
    fileActionSettings: {
        showRemove: false,
        showZoom: false,
        showDrag: false

    },
    maxFileSize: 20000,
    initialPreview: [initialPreviewTextData($("#doc-file").data('doc'))]
}).on("filebatchuploadsuccess", function(event, data, previewId, index) {
    $('[name="' +  $(this).data('field') + '"]').val(data.response.name);
}).on('filesuccessremove', function(event, id) {
    $('[name="' +  $(this).data('field') + '"]').val("");
}).on("filebatchselected", function(event, files) {
    $("#doc-file").fileinput("upload");
});

function initialPreviewImgData(obj) {
    if (typeof obj !== "undefined" && obj !== null && obj !== "") {
      return '<img src="/show/' + obj  + '" class="file-preview-image"/>';
    }
}

function initialPreviewTextData(obj) {
    if (typeof obj !== "undefined" && obj !== null && obj !== "") {
        return '<textarea title="' + obj  + '" class="file-preview-text">'+ obj  +'</textarea>';
    }
}




